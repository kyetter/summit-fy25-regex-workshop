# Summit 2024 Unconference: Regular Expressions

* [Unconference Issue](https://gitlab.com/gitlab-com/marketing/corporate_marketing/contribute/las-vegas-2024/-/issues/227)
* Slack channel #summit-fy25-regex-workshop
* [https://gitlab.com/kyetter/summit-fy25-regex-workshop]()

by Kyle Yetter

## The Repo

This repository contains the presentation and related content for my workshop on regular expressions, as well as
additional links to further text and tools to help you work with regex. 

### [The Slides](./unconference-fy25-regular-expressions.pptx)

This is the main presentation slide deck covered during the workshop

### [examples directory](./examples/)

Contains extra videos that show examples of applying regular expressions in day to day situations

### [exercises directory](./exercises/) 

Each subdirectory describes an exercise and provides example solution files to help
master more complex regular expressions

----

## Tools and Reference

### Official Engine References

#### [Ruby (3.2)](https://ruby-doc.org/3.2.2/Regexp.html)

#### [Python (3)](https://docs.python.org/3/library/re.html)

#### [JavaScript (From Mozilla)](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Regular_expressions)

### Cheat Sheets and General Reference

#### [Cheatography Printable Cheat Sheet](https://cheatography.com/davechild/cheat-sheets/regular-expressions/)

#### [Regular Expression Quick Ref: rexegg.com](https://www.rexegg.com/regex-quickstart.html)

### Online Testers

#### [regexr.com](https://regexr.com)

Amazing tool and reference set to compose a regular expression and test it against various input texts. It also links
to quick references, cheat sheets, and libraries of existing expressions.

#### [debuggex.com](https://debuggex.com)

A neat tool that will take an input regular expression and generate a graphical flow chart describing how this pattern
performs its matching. Think of it as a "visual explain" of a regular expression.

### Grepping (Searching Files)

On OSX, the standard system `grep` command does not support PCRE content. I recommend switching to one of these options instead:

#### [GNU grep](https://www.gnu.org/software/grep/)

`brew install grep`

This will install new command `ggrep`, which is GNU's version of the `grep` command. Pass the `-P` switch to
specify PCRE patterns.

```bash
# find all files containing words that end with "operator" in the app directory
ggrep -R -i -P 'operator\b' ./app
```

#### [ack: Programmer's grep](https://beyondgrep.com/)

`brew install ack`

This tool is an alternative to grep that formats matched patterns
much better and restricts the files it searches to common source code files in a directory tree. It console colorizes
the output to make it very clean and readable.

#### [ag (The Silver Searcher)](https://geoff.greer.fm/ag/)

`brew install the_silver_searcher`

A clone of `ack` written in C rather than Perl to make it run much much faster. `ack` itself is already
pretty swift, but if you are concerned about speed, this is the tool to replace it.

### Low Level Analysis and Code Tools

#### [re2c](https://re2c.org/)

Convert a regular expression into a low-level source code lexical analysis loop in C, Go, or Rust.
This is a somewhat complex tool that requires you to read the man page to use properly, but it gives
a lot of insight into the low level mechanics of text pattern matching.

#### [Regexp::Assemble (Perl)](https://metacpan.org/pod/Regexp::Assemble)

This is a semi-obscure Perl-based library tool that I have used in the past. If you have a long list of words that
you need to match, you cannot always just string them together with `|` between them and have it match exactly how
you want. This is a tool that lets you add a bunch of words and it will output an optimized regular expression that
factors out common characters.
