rx 1:
%r<
\b 
    (?:
      (?:
        2 (?: [0-4] \d | 5 [0-5] )
      | 1 \d \d
      | [1-9]? \d
      )
      \.
    ){3}
    (?:
      2 (?: [0-4] \d | 5 [0-5] )
    | 1 \d \d
    | [1-9]? \d
    )
\b
>x
